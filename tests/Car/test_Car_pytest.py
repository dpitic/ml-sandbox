import pytest

from mlsb.Car import Car


@pytest.fixture
def my_car():
    return Car(50)


def test_accelerate(my_car):
    my_car.accelerate()
    assert my_car.speed == 55


def test_brake():
    car = Car(50)
    car.brake()
    assert car.speed == 45
