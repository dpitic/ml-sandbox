"""Explore and plot sunspots data.
Ref: https://machinelearningmastery.com/backtest-machine-learning-models-time-series-forecasting/

The dataset contains the monthly count of the number of observed sunspots for
just over 230 years (1749-1983).  It contains 2820 observations.
"""

import matplotlib.pyplot as plt
import pandas as pd
from sklearn.model_selection import TimeSeriesSplit


def explore(filename, as_series=False):
    """Return a time series dataset.

    This function loads the raw data, displays a sample of the data, outputs
    descriptive statistics, plots the data and returns a DataFrame or Series
    of the raw data.

    :param str filename: Raw data file name.
    :param bool as_series: If true return data as a Series, otherwise as a
        DataFrame; default=False.
    :return: DataFrame or Series of the raw time series data.
    """
    series = pd.read_csv(filename, index_col=0, parse_dates=[0],
                         squeeze=as_series)
    print(series)
    print(series.describe())
    series.plot()
    plt.grid()
    return series


def main():
    filename = 'data/sunspots.csv'
    print('Raw sunspots data:')
    series = explore(filename, as_series=True)
    # Train/test split; using basic indexing
    train_size = int(len(series) * 0.66)
    train, test = series[:train_size], series[train_size:]
    print('\nTrain/test split using basic indexing:')
    print(f'Observations: {len(series)}')
    print(f'Training observations: {len(train)}')
    print(f'Testing observations: {len(test)}')
    plt.figure()
    plt.plot(train, label='Train')
    plt.plot(test, label='Test')
    plt.legend()
    plt.grid()
    # Multiple train/test split; using scikit-learn
    splits = TimeSeriesSplit(n_splits=3)
    plt.figure()
    index = 1
    print('\nTrain/test splits using TimeSeriesSplit():')
    for train_indices, test_indices in splits.split(series):
        train = series.iloc[train_indices]
        test = series.iloc[test_indices]
        print(f'Observations: {len(train) + len(test)}')
        print(f'Training observations: {len(train)}')
        print(f'Testing observations: {len(test)}')
        plt.subplot(310 + index)
        plt.plot(train)
        plt.plot(test)
        plt.grid()
        index += 1
    plt.show()
    # Walk forward validation
    train_size = 500
    num_records = len(series)
    train_test_list = []
    for i in range(train_size, num_records):
        train, test = series[:i], series[i:i + 1]
        #
        # Train and evaluate the model inside this loop
        #
        train_test_list.append((len(train), len(test)))
    train_test_df = pd.DataFrame(train_test_list, columns=['Train', 'Test'])
    print('\nWalk forward validation:')
    print(train_test_df)


if __name__ == '__main__':
    main()
