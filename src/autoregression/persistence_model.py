"""Persistence model for daily minimum temperatures data set.
Ref: https://machinelearningmastery.com/autoregression-models-time-series-forecasting-python/
"""
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.metrics import mean_squared_error

from wwcdml import shift


def main():
    filename = 'data/daily-min-temperatures.csv'
    series = pd.read_csv(filename, header=0, index_col=0, parse_dates=[0],
                         squeeze=True)
    # Create lagged data set for lag=1
    lagged_df = shift(series)
    # Split into train and test data sets; test=7 days
    test_size = 7
    train, test = lagged_df.iloc[:-test_size], lagged_df.iloc[-test_size:]
    train_x, train_y = train.iloc[:, 0], train.iloc[:, 1]
    test_x, test_y = test.iloc[:, 0], test.iloc[:, 1]
    # Walk forward validation
    predictions = test_x
    actual = test_y
    test_score = mean_squared_error(actual, predictions)
    print(f'Test MSE: {test_score}')
    # Plot predictions vs actual
    plt.plot(actual, label='Actual')
    plt.plot(predictions, label='Forecast')
    plt.legend()
    plt.grid()
    plt.show()


if __name__ == '__main__':
    main()
