"""Minimum Daily Temperatures Dataset
Ref: https://machinelearningmastery.com/autoregression-models-time-series-forecasting-python/

This dataset describes the minimum daily temperatures over 10 years (1981-1990)
in Melbourne, Australia.  The temperature units are degrees Celsius and there
are 3,650 observations.
"""

import matplotlib.pyplot as plt
import pandas as pd
from statsmodels.graphics.tsaplots import plot_acf

from wwcdml import shift


def main():
    filename = 'data/daily-min-temperatures.csv'
    series = pd.read_csv(filename, header=0, index_col=0, parse_dates=[0],
                         squeeze=True)
    print('Raw daily minimum temperatures:')
    print(series)
    series.plot()
    plt.grid()
    plt.figure()
    pd.plotting.lag_plot(series)
    plt.grid()
    # Pearson correlation coefficient for lag=1
    df = shift(series)
    print('Shifted DataFrame:')
    print(df)
    print('Pearson correlation coefficient:')
    pearson = df.corr()
    print(pearson)
    # Autocorrelation plot
    plt.figure()
    pd.plotting.autocorrelation_plot(series)
    plot_acf(series)
    plt.show()


if __name__ == '__main__':
    main()
