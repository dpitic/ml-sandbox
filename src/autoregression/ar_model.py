"""Autoregression model for daily minimum temperatures data set.
Ref: https://machinelearningmastery.com/autoregression-models-time-series-forecasting-python/
"""
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.metrics import mean_squared_error
from statsmodels.tsa.ar_model import AutoReg


def main():
    filename = 'data/daily-min-temperatures.csv'
    series = pd.read_csv(filename, header=0, index_col=0, parse_dates=[0],
                         squeeze=True)
    # Split into train and test data sets; test=7 days
    test_size = 7
    x = series.to_numpy()
    train, test = x[:-test_size], x[-test_size:]
    # Autoregression model
    print('Autoregression model:')
    model = AutoReg(train, lags=29)
    model_fit = model.fit()
    print('Coefficients')
    print(model_fit.params)
    # Make predictions
    predictions = model_fit.predict(start=len(train),
                                    end=len(train) + len(test) - 1,
                                    dynamic=False)
    # Evaluate model
    for i in range(len(predictions)):
        print(f'Predicted={predictions[i]:f}, Actual={test[i]:f}')
    error = mean_squared_error(test, predictions)
    print(f'Test MSE: {error}')
    # Plot results
    plt.plot(test, label='Actual')
    plt.plot(predictions, label='Forecast')
    plt.title('Autoregression Model')
    plt.legend()
    plt.grid()
    # Walk forward forecast
    print('Walk forward predictions:')
    window = 29
    coef = model_fit.params
    history = train[len(train) - window:]
    history = [history[i] for i in range(len(history))]
    predictions = list()
    for t in range(len(test)):
        length = len(history)
        lag = [history[i] for i in range(length - window, length)]
        pred = coef[0]
        for d in range(window):
            pred += coef[d + 1] * lag[window - d - 1]
        obs = test[t]
        predictions.append(pred)
        history.append(obs)
        print(f'Predicted={pred:f}, Actual={obs:f}')
    error = mean_squared_error(test, predictions)
    print(f'Test MSE: {error}')
    # Plot walk forward predictions
    plt.figure()
    plt.plot(test, label='Actual')
    plt.plot(predictions, label='Forecast')
    plt.title('Walk Forward Autoregression Model')
    plt.legend()
    plt.grid()
    plt.show()


if __name__ == '__main__':
    main()
