"""Transfer learning loop design."""


def eval_xfer_v1(num_fixed_layers, repeats=3, start_num_of_fixed_layers=0):
    """Original implementation."""
    for fixed_layers in range(start_num_of_fixed_layers, num_fixed_layers + 1):
        print(f'\nEvaluating transfer learning model for number of fixed '
              f'layers = {fixed_layers}')
        eval_xfer_model(fixed_layers)
        print(f'Saving model evaluation to: scores-{fixed_layers}.csv')
        print(f'Summary for model with number of fixed layers = '
              f'{fixed_layers}')


def eval_xfer_v2(iterations, repeats=3, start_num_of_fixed_layers=0):
    """Modified implementation."""
    for fixed_layers in range(start_num_of_fixed_layers,
                              start_num_of_fixed_layers + iterations):
        print(f'\nEvaluating transfer learning model for number of fixed '
              f'layers = {fixed_layers}')
        eval_xfer_model(fixed_layers)
        print(f'Saving model evaluation to: scores-{fixed_layers}.csv')
        print(f'Summary for model with number of fixed layers = '
              f'{fixed_layers}')


def eval_xfer_model(fixed_layers, repeats=3):
    """Original implementation."""
    for i in range(repeats):
        print(f'Training iteration {i + 1} of {repeats} with number of fixed '
              f'layers = {fixed_layers}')
        rnn_transfer(fixed_layers)


def rnn_transfer(fixed_layers):
    """Original implementation."""
    print('Number of fixed layers =', fixed_layers)
    for i in range(fixed_layers):
        print(f'\tLayer {i} trainable = False')


def main():
    fixed_layers = 1  # for rnn_transfer() only; removed from eval_xfer()
    start_num_of_fixed_layers = 1
    print('Original implementation')
    print('fixed_layers =', fixed_layers)
    print('start_num_of_fixed_layers =', start_num_of_fixed_layers)
    iterations = 2  # replace fixed_layers parameter in eval_xfer()
    print('number of iterations =', iterations)
    eval_xfer_v2(iterations=iterations,
                 start_num_of_fixed_layers=start_num_of_fixed_layers)


if __name__ == '__main__':
    main()
