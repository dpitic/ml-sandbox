"""Autoregressive Integrated Moving Average (ARIMA)

Ref: https://machinelearningmastery.com/arima-for-time-series-forecasting-with-python/
ARIMA model for shampoo sales dataset.
"""
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.metrics import mean_squared_error
from statsmodels.tsa.arima_model import ARIMA

import ms_ts_lstm.explore_raw_shampoo_sales_data as explore


def main():
    filename = 'data/shampoo-sales.csv'
    series = explore.load_raw_shampoo_sales_data(filename)
    # Fit ARIMA model
    p = 5  # number of lag observations
    d = 1  # number of times to difference (degree of differencing)
    q = 0  # size of moving average window
    model = ARIMA(series, order=(p, d, q))
    model_fit = model.fit(disp=0)
    print(model_fit.summary())
    # Plot residual errors
    residuals = pd.DataFrame(model_fit.resid)
    residuals.plot()
    plt.title('Residual Errors')
    plt.grid()
    plt.show()
    residuals.plot(kind='kde')
    plt.title('Residual Error Density')
    plt.grid()
    plt.show()
    print('Residuals summary:')
    print(residuals.describe())
    # Rolling forecast with ARIMA model
    values = series.to_numpy()
    # Split into training and test data sets; training size = 66%
    size = int(len(values) * 0.66)
    train, test = values[0:size], values[size:]
    history = train.tolist()
    predictions = list()
    # Forecast test data
    for t in range(len(test)):
        # Recreating the model each time is crude; must be better ways
        model = ARIMA(history, order=(p, d, q))
        model_fit = model.fit(disp=0)
        output = model_fit.forecast()
        forecast = output[0][0]
        predictions.append(forecast)
        observation = test[t]
        history.append(observation)
        print(f'Predicted: {forecast:f}, Expected: {observation:f}')
    # Evaluate predictions
    error = mean_squared_error(test, predictions)
    print(f'Test MSE: {error}')
    # Plot forecast predictions
    plt.figure()
    plt.plot(test, label='Actual (test)')
    plt.plot(predictions, label='Forecast')
    plt.legend()
    plt.grid()
    plt.show()


if __name__ == '__main__':
    main()
