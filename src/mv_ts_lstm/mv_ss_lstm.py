"""Multi-variate single-step LSTM model.

This script is used to prepare the pollution data set for LSTM.  The supervised
learning problem will be framed to predict the pollution at the current hour (t)
given the pollution measurement and weather conditions at the prior time step.
"""
import math
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import MinMaxScaler

import explore_raw_beijing_pollution_data as explore
import wwcdml


def main():
    filename = 'data/pollution.csv'
    dataset = explore.preprocess_data(filename)
    values = dataset.to_numpy()
    # Integer encode wind direction
    encoder = LabelEncoder()
    values[:, 4] = encoder.fit_transform(values[:, 4])
    # Ensure all data is float
    values = values.astype('float32')
    # Normalise features
    scaler = MinMaxScaler(feature_range=(0, 1))
    scaled = scaler.fit_transform(values)
    # Frame data as supervised learning
    lag = 1  # use 1 previous time step for prediction
    forecast = 1  # predict 1 time step
    reframed = wwcdml.sliding_window(scaled, lag, forecast)
    # Drop columns we don't want to predict
    reframed.drop(reframed.columns[[9, 10, 11, 12, 13, 14, 15]], axis=1,
                  inplace=True)
    pd.set_option('display.width', None)
    print(reframed.head())
    # Split train and test data; train = 1st year; test = remaining 4 years
    values = reframed.to_numpy()
    n_train_hours = 365 * 24  # training data is 1st year data
    train = values[:n_train_hours, :]
    test = values[n_train_hours:, :]
    # Split into samples and targets
    train_x, train_y = train[:, :-1], train[:, -1]
    test_x, test_y = test[:, :-1], test[:, -1]
    # Reshape input samples to be 3D: [samples, timesteps=lag=1, features]
    train_x = train_x.reshape(train_x.shape[0], lag, train_x.shape[1])
    test_x = test_x.reshape(test_x.shape[0], lag, test_x.shape[1])
    print(train_x.shape, train_y.shape, test_x.shape, test_y.shape)
    # LSTM model architecture; input shape [timestep=1, features=8]
    model_path = Path('models/mv_ss_lstm.h5')
    if model_path.exists():
        # Load existing model
        print(f'Model file {model_path} found; loading model ...')
        model = tf.keras.models.load_model(model_path)
    else:
        # Build and compile the model
        print(f'Model file {model_path} not found; creating and training ...')
        model = tf.keras.Sequential()
        # Input layer; shape: [timesteps=1, features=8]
        model.add(tf.keras.layers.LSTM(
            units=50, input_shape=(train_x.shape[1], train_x.shape[2])))
        # Output layer; predict 1 variable only (pollution)
        model.add(tf.keras.layers.Dense(1))
        model.compile(loss='mae', optimizer='adam')
        # Fit model; batch size 3 days = 3 days * 24 hours = 72 hours
        history = model.fit(train_x, train_y, epochs=50, batch_size=3 * 24,
                            validation_data=(test_x, test_y), shuffle=False)
        # Save the entire model in HD5 format
        print('Training complete. Saving entire model as', model_path)
        model.save(model_path)
        # Plot model training history: training & test losses
        plt.plot(history.history['loss'], label='train')
        plt.plot(history.history['val_loss'], label='test')
        plt.legend()
        plt.grid()
        plt.show()
    # Print model summary
    print(model.summary())
    # Evaluate model on test data
    predictions = model.predict(test_x)
    test_x = test_x.reshape(test_x.shape[0], test_x.shape[2])
    # Invert scaling for predictions
    inv_preds = np.concatenate((predictions, test_x[:, 1:]), axis=1)
    inv_preds = scaler.inverse_transform(inv_preds)
    inv_preds = inv_preds[:, 0]
    # Invert scaling for actual test data
    test_y = test_y.reshape(len(test_y), 1)
    inv_actual = np.concatenate((test_y, test_x[:, 1:]), axis=1)
    inv_actual = scaler.inverse_transform(inv_actual)
    inv_actual = inv_actual[:, 0]
    # Calculate RMSE
    rmse = math.sqrt(mean_squared_error(inv_actual, inv_preds))
    print(f'Test RMSE: {rmse}')


if __name__ == '__main__':
    main()
