"""Explore the raw Bejing pollution data.

The dataset contains weather reports and pollution level for each hour for five
years at the US embassy in Beijing, China.

Ref: https://machinelearningmastery.com/multivariate-time-series-forecasting-lstms-keras/
"""
import matplotlib.pyplot as plt
import pandas as pd


def _parser(x):
    """Return a Pandas datetime object from the raw data value.

    This function is the default datetime parser for the raw Beijing pollution
    data set.  It's intended purpose is to be used as the datetime parser
    when reading the raw data set.
    :param str x: Raw data file datetime value.
    :return any: Pandas datetime object.
    """
    return pd.datetime.strptime(x, '%Y %m %d %H')


def preprocess_data(filename, date_parser=_parser):
    """Return a Pandas DataFrame containing the prepared Beijing pollution data.

    This function reads the specified raw data CSV file and preprocesses the
    data to clean and parse the datetime column.
    :param str filename: Raw data file name.
    :param any date_parser: Datetime parser object.
    :return: Pandas DataFrame containing preprocessed data.
    """
    dataset = pd.read_csv(filename, index_col=0,
                          parse_dates=[['year', 'month', 'day', 'hour']])
    dataset.drop('No', axis=1, inplace=True)
    # Rename columns to be more descriptive
    dataset.columns = ['pollution', 'dew', 'temp', 'press', 'wnd_dir',
                       'wnd_spd', 'snow', 'rain']
    dataset.index.name = 'date'
    # Change all pollution NA values to 0
    dataset['pollution'].fillna(0, inplace=True)
    # Drop the first 24 hours which has no pollution data
    dataset = dataset[24:]
    return dataset


def plot_data(dataset):
    """Plot Beijing pollution data.

    This function plots each series as a separate subplot except for the wind
    direction which is categorical data.
    :param DataFrame dataset: Data set.
    """
    values = dataset.to_numpy()
    # Columns to plot
    groups = [0, 1, 2, 3, 5, 6, 7]
    i = 1
    # Plot each column
    plt.figure()
    for group in groups:
        plt.subplot(len(groups), 1, i)
        plt.plot(values[:, group])
        plt.title(dataset.columns[group], y=0.5, loc='right')
        i += 1
    plt.grid()
    plt.show()


def main():
    filename = 'data/pollution.csv'
    dataset = preprocess_data(filename)
    # Summarise first 5 rows
    print(dataset.head(5))
    plot_data(dataset)


if __name__ == '__main__':
    main()
