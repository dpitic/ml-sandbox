"""Collection of various utility functions for the module."""
import pandas as pd
import tensorflow as tf
from matplotlib import pyplot as plt

import seqlib


def difference(dataset, interval=1):
    """Return a difference series.

    This function calculates a difference series on the dataset using the
    specified interval by subtracting the previous interval time step value
    from the current time step value.
    :param dataset: Pandas series containing the raw shampoo sales data set.
    :param int interval: Differencing window size.
    :return: Pandas series containing stationary data set.
    """
    diff = list()
    for i in range(interval, len(dataset)):
        value = dataset[i] - dataset[i - interval]
        diff.append(value)
    return pd.Series(diff)


def plot_forecasts(series, forecasts, test_size, figure_path=None):
    """Plot the forecasts in the context of the original data set.

    :param DataFrame series: Original time series data.
    :param list forecasts: List of forecasts.
    :param int test_size: Number of samples in the test data set.
    :param str figure_path: Path and file name of figure to save, default=None
    """
    series_values = series.to_numpy()
    plt.figure()
    plt.plot(series_values, label='Actual')
    # Plot the forecasts
    for i in range(len(forecasts)):
        start_offset = len(series) - test_size + i - 1
        end_offset = start_offset + len(forecasts[i]) + 1
        xaxis = [x for x in range(start_offset, end_offset)]
        yaxis = [series_values[start_offset]] + forecasts[i]
        plt.plot(xaxis, yaxis, color='red', label='Forecast')
    plt.legend()
    plt.grid()
    seqlib.save_figure(figure_path)


class LossHistory(tf.keras.callbacks.Callback):
    """Custom callback to record training history.

    This class is used when the model is fit with 1 epoch per batch and the
    fit method is executed for the total number of epochs.  This often occurs
    when fitting stateful LSTM models where the state must be reset between
    each epoch.
    """

    def __init__(self):
        super().__init__()
        self.history = {'loss': []}

    def on_epoch_end(self, epoch, logs={}):
        loss = logs.get('loss')
        self.history['loss'].append(loss)
