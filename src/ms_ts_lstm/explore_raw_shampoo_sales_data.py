"""Explore the raw shampoo sales data.

The dataset contains the monthly number of sales of shampoo over a 3 year
period.

Ref: https://machinelearningmastery.com/multi-step-time-series-forecasting-long-short-term-memory-networks-python/
"""
import matplotlib.pyplot as plt
import pandas as pd
from pandas.plotting import autocorrelation_plot

import utils


def _parser(x):
    """Return a Pandas datetime object from the raw shampoo dataset.

    This function is the default datetime parser for the raw shampoo dateset.
    It's intended purpose is to be used as the datetime parser when reading
    the raw dataset.
    :param str x: Raw data file datetime value.
    :return any: Pandas datetime object.
    """
    return pd.datetime.strptime('190' + x, '%Y-%m')


def load_raw_shampoo_sales_data(filename, date_parser=_parser):
    """Return a Pandas Series containing the raw shampoo sales data.

    This function reads the specified CSV file and parses the first column
    as a datetime column according to the specified date parser.
    :param str filename: Raw data file name.
    :param any date_parser: Datetime parser object.
    :return: Pandas Series containing the raw shampoo sales data.
    """
    return pd.read_csv(filename, header=0, parse_dates=[0],
                       index_col=0, squeeze=True, date_parser=date_parser)


def plot_raw_shampoo_sales_data(series):
    """Plot the raw shampoo sales data.

    This function plots the raw shampoo sales data provided by the specified
    Pandas series object.  It is expected the series contains the parsed
    datetime column.
    :param any series: Pandas series of raw shampoo sales data.
    """
    plt.figure()
    series.plot()
    plt.title('Monthly Shampoo Sales')
    plt.ylabel('Sales Numbers')
    plt.grid()


def plot_difference_series(series):
    """Plot the difference series."""
    plt.figure()
    series.plot(label='Custom diff')
    plt.title('Difference Series')
    plt.grid()


def main():
    filename = 'data/shampoo-sales.csv'
    series = load_raw_shampoo_sales_data(filename)
    # Summarise first few rows
    print(series.head())
    # Plot raw data
    plot_raw_shampoo_sales_data(series)
    # Calculate and plot 1st difference series (using custom function)
    diff = utils.difference(series)
    plot_difference_series(diff)
    # Use Pandas diff() method
    pandas_diff = series.diff()
    pandas_diff.dropna(inplace=True)
    pandas_diff = pd.Series(pandas_diff.to_numpy(),
                            index=list(range(len(pandas_diff))))
    plt.plot(pandas_diff, label='Pandas diff')
    plt.legend()
    # Pandas diff() method maintains the date/time index
    plt.figure()
    plt.plot(series.diff(), label='Pandas diff')
    plt.grid()
    # Autocorrelation plot
    plt.figure()
    autocorrelation_plot(series)
    plt.show()


if __name__ == '__main__':
    main()
