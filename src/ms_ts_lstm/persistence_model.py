"""Persistence model for shampoo sales data.
Ref: https://machinelearningmastery.com/multi-step-time-series-forecasting-long-short-term-memory-networks-python/

The Persistence Model provides a simple baseline for time series forecasting,
often called the naive forecast.  This is a forecasting model where the last
observation is persisted forward.  This example uses the first two years of
data for training and the remaining one year for test data.  The model are used
to make predictions on the test data set (final year of data).  The multi-step
forecast is required to make a 3 month forecast for a given month in the final
12 months of the data set.
"""
import math

import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error

import explore_raw_shampoo_sales_data as explore
import utils
import wwcdml


def time_series_to_supervised(series, lag, forecast):
    """Return supervised learning data set DataFrame.

    This function converts the Pandas series time series data to a supervised
    learning data set DataFrame.
    :param series: Pandas series containing the raw data.
    :param int lag: Number of time steps to use for prediction.
    :param int forecast: Number of time steps to forecast.
    :return DataFrame: Supervised learning data set.
    """
    raw_values = series.to_numpy()
    # Reshape to column vector
    raw_values = raw_values.reshape(len(raw_values), 1)
    # Transform time series to supervised learning problem: x, y
    supervised = wwcdml.sliding_window(raw_values, lag, forecast)
    return supervised


def _persistence(last, future):
    """Return persistence forecast from the last observation.

    This function takes the last observation and returns its value as the
    persistence forecast for the specified number of future time steps.  This
    means that the forecast for all future time steps is the constant value of
    the last observation.
    :param int last: Last observation.
    :param int future: Number of future time steps to forecast.
    :return list: A list of persistence forecast values for the last observation
        value.
    """
    return [last for i in range(future)]


def persistence_forecasts(data, lag, future):
    """Return list of persistence forecasts for data.

    This function uses the lag number of observations of data to return the
    persistence forecast for the specified number of future time steps.
    :param ndarray data: Data matrix.
    :param int lag: Number of lag observations to use for forecasting.
    :param int future: Number of future time steps to forecast.
    :return list: List of persistence forecasts.
    """
    forecasts = list()
    for i in range(len(data)):
        x, y = data[i, 0:lag], data[i, lag:]
        # Make forecasts
        forecast = _persistence(x[-1], future)
        # Store forecast
        forecasts.append(forecast)
    return forecasts


def evaluate_forecasts(test, forecasts, lag, future, verbose=False):
    """Return list of root mean squared error forecast evaluations.

    This function evaluates the forecasts against the test data and calculates
    the root mean squared error (RMSE) for each forecast.
    :param ndarray test: Test data matrix.
    :param list forecasts: List of forecasts.
    :param int lag: Number of lag observations used in forecasting.
    :param int future: Number of future time steps forecast.
    :param bool verbose: Output evaluation values, default=False.
    :return list: List of RMSE values.
    """
    evaluations = list()
    for i in range(future):
        actual = test[:, (lag + i)]
        predicted = [forecast[i] for forecast in forecasts]
        rmse = math.sqrt(mean_squared_error(actual, predicted))
        evaluations.append(rmse)
        if verbose:
            print(f't+{i + 1} RMSE: {rmse}')
    return evaluations


def main():
    filename = 'data/shampoo-sales.csv'
    series = explore.load_raw_shampoo_sales_data(filename)
    print('Raw data set:')
    print(series)
    # Configure model
    lag = 1  # 1 previous month time series
    future = 3  # forecast 3 months into the future
    # Prepare data for supervised learning
    data = time_series_to_supervised(series, lag, future)
    print('Complete supervised learning data set:')
    print(data)
    data = data.to_numpy()
    # Split training and test data; last 10 values are training data
    test_size = 10
    train, test = data[0: -test_size], data[-test_size:]
    print('Test data matrix:')
    print(test)
    print(f'Train: {train.shape}, Test: {test.shape}')
    # Make forecasts
    forecasts = persistence_forecasts(test, lag, future)
    # Evaluate forecasts
    evaluate_forecasts(test, forecasts, lag, future, verbose=True)
    # Plot persistence forecasts for last 12 months
    utils.plot_forecasts(series, forecasts, test_size + 2)
    plt.title('Persistence Forecast for Last 12 Months')
    plt.show()


if __name__ == '__main__':
    main()
