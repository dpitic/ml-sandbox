"""Multi-step LSTM model with state.
Ref: https://machinelearningmastery.com/multi-step-time-series-forecasting-long-short-term-memory-networks-python/

This script is used to fit an LSTM model to the training data and make
multi-step forecasts for the test data.  For this to happen the data must be
made stationary to remove the increasing trend by differencing, and the data
must be scaled to values between -1 and 1 for compatibility with the activation
function of the LSTM units.
"""
import math
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import MinMaxScaler

import explore_raw_shampoo_sales_data as explore
import utils
import wwcdml


def time_series_to_supervised(series, lag, forecast, scaler):
    """Return supervised learning data set DataFrame.

    This function converts the Pandas series time series data to a supervised
    learning data set DataFrame.  In order to prepare the data for training
    LSTM models, it removes the trend by differencing, and normalises the data
    by scaling using the specified scaler.
    :param series: Pandas series containing the raw time series data.
    :param int lag: Number of time steps to use for prediction.
    :param int forecast: Number of time steps to forecast.
    :param scaler: Normalisation object used to scale the data.
    :return DataFrame: Supervised learning data set.
    """
    raw_values = series.to_numpy()
    # Transform data to be stationary (remove trend)
    diff_series = utils.difference(raw_values, 1)
    diff_values = diff_series.to_numpy()
    # Reshape to column vector
    diff_values = diff_values.reshape(len(diff_values), 1)
    # Normalise data and reshape to column vector
    scaled_values = scaler.fit_transform(diff_values)
    scaled_values = scaled_values.reshape(len(scaled_values), 1)
    # Transform to supervised learning problem: samples and targets DataFrame
    supervised = wwcdml.sliding_window(scaled_values, lag, forecast)
    return supervised


def build_model(train_x, train_y, neurons, batch_size, lag=1, stateful=False):
    """Return an LSTM network model (not compiled).

    This function builds an LSTM network architecture model which is not
    compiled.
    :param ndarray train_x: Training sample data; shape [samples, timesteps]
    :param ndarray train_y: Training target data; shape [samples, forecasts]
    :param int neurons: Number of LSTM units.
    :param int batch_size: Batch size.
    :param int lag: Number of input time steps, default=1
    :param bool stateful: Flag to control model stateful attribute,
        default=False.
    :return: LSTM model (not compiled).
    """
    # Reshape training data to [samples, timesteps=lag=1, features=1]
    train_x = train_x.reshape(train_x.shape[0], lag, train_x.shape[1])
    # LSTM network architecture
    model = tf.keras.Sequential()
    model.add(tf.keras.layers.LSTM(
        neurons,
        batch_input_shape=(batch_size, train_x.shape[1], train_x.shape[2]),
        stateful=stateful)
    )
    # Output layer neurons = number of forecast time steps
    model.add(tf.keras.layers.Dense(train_y.shape[1]))
    return model


def _forecast_lstm(model, x, batch_size=None):
    """Return array of forecasts.

    This is a convenience function for making predictions on the model.
    :param model: LSTM model for prediction.
    :param ndarray x: Input samples for prediction.
    :param int or None batch_size: Batch size for prediction.
    :return list: List of forecasts.
    """
    # Reshape input to [samples=1, timesteps=1, features]
    x = x.reshape(1, 1, len(x))
    # Make forecast predictions
    forecast = model.predict(x, batch_size=batch_size)
    # Convert to array
    return [x for x in forecast[0, :]]


def make_lstm_forecasts(model, data, lag, batch_size=None):
    """Return a list of forecast predictions on data.

    This function uses the model to produce forecast predictions on the data.
    :param model: LSTM model for prediction.
    :param ndarray data: Input data matrix.
    :param int or None batch_size: Batch size for prediction.
    :param int lag: Number of observations used in forecasting.
    :return list: List of forecasts.
    """
    forecasts = list()
    for i in range(len(data)):
        x, y = data[i, 0:lag], data[i, lag:]
        # Make forecast
        forecast = _forecast_lstm(model, x, batch_size)
        # Store forecast
        forecasts.append(forecast)
    return forecasts


def _inverse_difference(last, forecast, interval=1):
    """Return list of inverse difference of last observation and forecast value.

    This function inverses the differencing operation applied to the data
    which was necessary to remove the trend.  The difference inversion is
    performed by adding the value of the last observation (prior months'
    shampoo sales) to the first forecast value, and then propagating the
    value down the forecasts.
    :param last: Last observation value.
    :param forecast: Forecast predictions based on last observation.
    :param int interval: Difference interval.
    :return list: List of inverse differences.
    """
    # Invert first forecast
    inverted = list()
    inverted.append(forecast[0] + last)
    # Propagate difference forecast using inverted first value
    for i in range(1, len(forecast)):
        inverted.append(forecast[i] + inverted[i - interval])
    return inverted


def inverse_transform(series, forecasts, scaler, test_size, diff_interval=1):
    """Return inverse difference and inverse scaled forecasts.

    This function inverses the difference and the normalisation scale of the
    forecast data.
    :param series: Original time series data.
    :param list forecasts: List of forecast predictions.
    :param scaler: Normalisation scaler used to scale the data.
    :param int test_size: Number of samples in the test data.
    :param int diff_interval: Difference interval, default=1.
    :return list: List of inverted forecast predictions.
    """
    inverted = list()
    data = series.to_numpy()
    for i in range(len(forecasts)):
        # Extract a forecast prediction array
        forecast = np.array(forecasts[i])
        # Reshape to row vector to invert scaling
        forecast = forecast.reshape(1, len(forecast))
        # Invert scaling of forecast value
        inv_scale_forecast = scaler.inverse_transform(forecast)
        inv_scale_forecast = inv_scale_forecast[0, :]
        # Invert differencing forecast
        index = len(series) - test_size + i - 1
        last = data[index]
        inv_diff_forecast = _inverse_difference(last, inv_scale_forecast,
                                                diff_interval)
        # Store inverted differencing forecast
        inverted.append(inv_diff_forecast)
    return inverted


def evaluate_forecasts(test, forecasts, future, verbose=False):
    """Return list of root mean square error forecast evaluations.

    This function evaluates the forecasts against the test data and calculates
    the root mean square error (RMSE) for each forecast.
    :param ndarray test: Test data matrix.
    :param list forecasts: List of forecasts (inverse difference and original
        scale).
    :param int future: Number of future time steps forecast.
    :param bool verbose: Output evaluation values, default=False.
    :return list: List of RMSE values."""
    evaluations = list()
    for i in range(future):
        actual = [row[i] for row in test]
        predicted = [forecast[i] for forecast in forecasts]
        rmse = math.sqrt(mean_squared_error(actual, predicted))
        evaluations.append(rmse)
        if verbose:
            print(f't+{i + 1} RMSE: {rmse}')
    return evaluations


def main():
    filename = 'data/shampoo-sales.csv'
    series = explore.load_raw_shampoo_sales_data(filename)
    print('Raw data set:')
    print(series)
    # Configure model
    lag = 1  # use 1 previous month time series for predictions
    future = 3  # forecast 3 months into the future
    # Prepare data for supervised learning
    scaler = MinMaxScaler(feature_range=(-1, 1))
    data = time_series_to_supervised(series, lag, future, scaler)
    data = data.to_numpy()
    # Split training and test data; last 10 values are test data
    test_size = 10
    train, test = data[0:-test_size], data[-test_size:]
    # Split training data into sample and target data sets
    train_x, train_y = train[:, 0:lag], train[:, lag:]
    # Try to load the model (if it exists)
    model_path = Path('models/ms_lstm.h5')
    if model_path.exists():
        # Load existing model
        print(f'Model file {model_path} found; loading model ...')
        model = tf.keras.models.load_model(model_path)
    else:
        # Build, compile and train the model
        print(f'Model file {model_path} not found; creating and training ...')
        neurons = 1
        batch_size = 1
        model = build_model(train_x, train_y, neurons, batch_size, lag,
                            stateful=True)
        model.compile(loss='mean_squared_error', optimizer='adam')
        # Fit model
        epochs = 1500
        history = utils.LossHistory()
        for i in range(epochs):
            print(f'Epoch {i + 1}/{epochs}')
            model.fit(train_x.reshape(train_x.shape[0], lag, train_x.shape[1]),
                      train_y, batch_size, epochs=1, shuffle=False,
                      callbacks=[history])
            model.reset_states()
        # Plot model training history
        wwcdml.plot_training_history(history, model_path.as_posix())
        # Save the entire model in HD5 format
        print('Training complete.  Saving entire model as', model_path)
        model.save(model_path)
    # Print model summary
    print(model.summary())
    # Make forecasts
    forecasts = make_lstm_forecasts(model, test, lag)
    # Invert transformations back to original scale for last 12 months
    inv_forecasts = inverse_transform(series, forecasts, scaler, test_size + 2)
    # Invert transformation on the output part of the test data set to correctly
    # calculate the RMSE scores
    actual = [row[lag:] for row in test]
    actual = inverse_transform(series, actual, scaler, test_size + 2)
    # Evaluate forecasts
    evaluate_forecasts(actual, inv_forecasts, future, verbose=True)
    # Plot forecasts for last 12 months
    figure_path = 'predictions/ms_lstm_forecast.png'
    utils.plot_forecasts(series, inv_forecasts, test_size + 2, figure_path)
    plt.title('LSTM Forecasts for last 12 Months')
    plt.show()


if __name__ == '__main__':
    main()
