"""Persistence model for shampoo sales data.
Ref: https://machinelearningmastery.com/time-series-forecasting-long-short-term-memory-network-python/

The Persistence Model Forecast provides a good baseline forecast for a time
series with a linear increasing trend.  The persistence forecast model uses the
observation from the prior time step (t-1) to predict the observation at the
current time step (t).
"""
import math

import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error

import ms_ts_lstm.explore_raw_shampoo_sales_data as explore


def persistence_forecasts(data, value):
    """Return list of persistence forecasts for given data.

    This function calculates the persistence forecast for the next time step of
    the specified data set starting with the specified last value.
    :param array-like data: Data to base forecasts on.
    :param value: First value to persist.
    :return list: Persistence forecasts.
    """
    history = [value]
    forecasts = list()
    for i in range(len(data)):
        # Make prediction forecast
        forecasts.append(history[-1])
        # Provide actual (test) observation for next prediction forecast
        history.append(data[i])
    return forecasts


def main():
    filename = 'data/shampoo-sales.csv'
    series = explore.load_raw_shampoo_sales_data(filename)
    print(series.head())
    explore.plot_raw_shampoo_sales_data(series)
    plt.show()
    # Split training and test data; last 12 month are test data
    test_size = 12  # months
    samples = series.to_numpy()
    train, test = samples[0:-test_size], samples[-test_size:]
    # Walk forward validation from last training data point
    forecasts = persistence_forecasts(test, train[-1])
    # Evaluate prediction model performance; actual = test data
    rmse = math.sqrt(mean_squared_error(test, forecasts))
    print(f'Test RMSE: {rmse}')
    # Plot actual (test) and forecast data
    plt.plot(test, label='Actual (test)')
    plt.plot(forecasts, label='Prediction')
    plt.legend()
    plt.grid()
    plt.show()


if __name__ == '__main__':
    main()
