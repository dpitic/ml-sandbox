"""Single-step univariate time series LSTM model with state.
Ref: https://machinelearningmastery.com/time-series-forecasting-long-short-term-memory-network-python/
"""
import math
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow as tf
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import MinMaxScaler

import ms_ts_lstm.explore_raw_shampoo_sales_data as explore
import ms_ts_lstm.utils as utils
import seqlib
import wwcdml


def time_series_to_supervised(data, lag=1, forecast=1, algorithm='custom'):
    """Return supervised learning data set DataFrame.

    This function converts the Pandas series time series data to a supervised
    learning data set DataFrame.
    :param array-like data: Pandas series containing the raw time series data.
    :param int lag: Number of time steps to use for prediction, default=1.
    :param int forecast: Number of time steps to forecast, default=1.
    :param algorithm: 'sliding' = sliding window; 'custom' = custom algorithm.
    :return: DataFrame containing supervised learning data set.
    """
    df = None
    if algorithm == 'custom':
        # Custom algorithm from reference web article
        df = pd.DataFrame(data)
        columns = [df.shift(i) for i in range(1, lag + 1)]
        columns.append(df)
        df = pd.concat(columns, axis=1)
        df.fillna(0, inplace=True)
    elif algorithm == 'sliding':
        # Sliding window algorithm
        samples = data.reshape(data.shape[0], 1)
        df = wwcdml.sliding_window(samples, lag, forecast)
    return df


def build_model(train_x, train_y, neurons, batch_size, lag=1, stateful=False):
    """Return an LSTM network model (not compiled).

    This function builds an LSTM network architecture model which is not
    compiled.
    :param array-like train_x: Training sample data; shape [samples, timesteps].
    :param array-like train_y: Training target data; shape [samples, forecasts].
    :param int neurons: Number of LSTM units.
    :param int batch_size: Batch size.
    :param int lag: Number of input time steps, default=1.
    :param bool stateful: Flag to control model stateful attribute,
        default=False.
    :return: LSTM model (not compiled).
    """
    # Reshape training data to [samples, timesteps=lag, features]
    train_x = train_x.reshape(train_x.shape[0], lag, train_x.shape[1])
    # LSTM network architecture
    model = tf.keras.Sequential()
    model.add(tf.keras.layers.LSTM(
        neurons,
        batch_input_shape=(batch_size, train_x.shape[1], train_x.shape[2]),
        stateful=stateful)
    )
    # Output layer neurons = number of forecast time steps
    model.add(tf.keras.layers.Dense(train_y.shape[1]))
    return model


def lstm_forecast(model, data, batch_size=None):
    """Make a one-step forecast."""
    x = data.reshape(1, 1, len(data))
    pred = model.predict(x, batch_size=batch_size)
    return pred[0, 0]


def _inverse_difference(history, diff, interval=1):
    """Return an inverse difference value.

    Helper function used to calculate inverse difference of a series.
    :param array-like history: Original series.
    :param diff: Difference value.
    :param int interval: Difference interval, default=1
    :return: Inverse difference value.
    """
    return diff + history[-interval]


def inverse_difference(series, difference):
    """Return an inverse difference series.

    This function is used to invert the difference function performed on data.
    :param array-like series: Original series.
    :param array-like difference: Difference series.
    :return: Pandas series containing inverse series.
    """
    inverted = list()
    for i in range(len(difference)):
        value = _inverse_difference(series, difference[i], len(series) - i)
        inverted.append(value)
    return pd.Series(inverted)


def inverse_scale(scaler, input, forecasts):
    """Inverse scaling for a forecast value."""
    new_row = [x for x in input] + [forecasts]
    ar = np.array(new_row)
    ar = ar.reshape(1, len(ar))
    inverted = scaler.inverse_transform(ar)
    return inverted[0, -1]


def sanity_check(train, test, raw_values, scaler):
    """Sanity check transforms and inverse transforms.

    If everything is as expected, this function should produce a model with
    perfect skill (i.e. a model that predicts the expected outcome).
    """
    predictions = list()
    for i in range(len(test)):
        # Make one-step forecast
        x, y = test[i, 0:-1], test[i, -1]
        # Perfect model
        pred = y
        # Invert scaling
        pred = inverse_scale(scaler, x, pred)
        # Invert differencing
        pred = _inverse_difference(raw_values, pred, len(test) + 1 - i)
        # Store forecasts
        predictions.append(pred)
        expected = raw_values[len(train) + i + 1]
        print(f'Month={i + 1:2d} Predicted={pred:f} Expected={expected:f}')
    # Evaluate the model
    rmse = math.sqrt(mean_squared_error(raw_values[-len(test):], predictions))
    print(f'Test RMSE: {rmse}')


def main():
    # Output TensorFlow and Keras version
    print('TensorFlow version:', tf.__version__)
    print('TensorFlow Keras version:', tf.keras.__version__)
    filename = 'data/shampoo-sales.csv'
    series = explore.load_raw_shampoo_sales_data(filename)
    # Difference data to be stationary (remove increasing trend)
    raw_values = series.to_numpy()
    diff_values = utils.difference(raw_values)
    # Transform data to supervised learning with lag and forecast both = 1
    supervised = time_series_to_supervised(diff_values.to_numpy())
    supervised_values = supervised.to_numpy()
    # Split training and test data; last 12 months are test data
    test_size = 12  # last 12 months are test data
    train = supervised_values[0: -test_size]
    test = supervised_values[-test_size:]
    # Normalise data scale using training data to reduce any bias
    scaler = MinMaxScaler(feature_range=(-1, 1))
    train_scaled = scaler.fit_transform(
        train.reshape(train.shape[0], train.shape[1]))
    test_scaled = scaler.transform(
        test.reshape(test.shape[0], test.shape[1]))
    # Try to load the model (if it exists)
    model_path = Path('models/ss_uv_ts_lstm.h5')
    batch_size = 1
    lag = 1
    if model_path.exists():
        # Load existing model
        print(f'Model file {model_path} found; loading model ...')
        model = tf.keras.models.load_model(model_path)
    else:
        # Build, compile and train the model
        print(f'Model file {model_path} not found; creating and training ...')
        neurons = 4
        train_x, train_y = train_scaled[:, 0:lag], train_scaled[:, lag:]
        model = build_model(train_x, train_y, neurons, batch_size, lag,
                            stateful=True)
        model.compile(loss='mean_squared_error', optimizer='adam')
        # Fit model
        epochs = 3000
        history = utils.LossHistory()
        for i in range(epochs):
            print(f'Epoch {i + 1}/{epochs}')
            model.fit(train_x.reshape(train_x.shape[0], lag, train_x.shape[1]),
                      train_y, batch_size, epochs=1, shuffle=False,
                      callbacks=[history])
            model.reset_states()
        # Plot model training history
        wwcdml.plot_training_history(history, model_path.as_posix())
        # Save the entire model in HD5 format
        print('Training complete. Saving entire model as', model_path)
        model.save(model_path)
    # Print model summary
    print(model.summary())
    # Forecast the entire training dataset to build up state for forecasting
    train_reshaped = train_scaled[:, 0].reshape(len(train_scaled), lag, 1)
    model.predict(train_reshaped)
    # Walk-forward validation on the test data (last 12 months of data)
    predictions = list()
    for i in range(len(test_scaled)):
        # Make one-step forecast
        x, y = test_scaled[i, 0:-1], test_scaled[i, -1]
        pred = lstm_forecast(model, x, batch_size=batch_size)
        # Invert scaling
        pred = inverse_scale(scaler, x, pred)
        # Invert differencing
        pred = _inverse_difference(raw_values, pred, len(test_scaled) + 1 - i)
        # Store forecasts
        predictions.append(pred)
        expected = raw_values[len(train) + i + 1]
        print(f'Month={i + 1:2d} Predicted={pred:f} Expected={expected:f}')
    # Evaluate model
    rmse = math.sqrt(mean_squared_error(raw_values[-test_size:], predictions))
    print(f'Test RMSE: {rmse}')
    # Plot actual vs predicted
    plt.figure()
    plt.plot(raw_values[-test_size:], label='Actual (test)')
    plt.plot(predictions, label='Forecast')
    plt.legend()
    plt.grid()
    # Save the prediction forecast plot
    seqlib.save_figure('predictions/ss_uv_ts_lstm.png')
    plt.show()
    # Sanity check model
    sanity_check(train, test_scaled, raw_values, scaler)


if __name__ == '__main__':
    main()
